import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfilesComponent implements OnInit {

  profiles: object;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get('https://api.myjson.com/bins/89fy7').subscribe(profiles => {
      this.profiles = profiles;
      console.log(this.profiles);
    });
  }

}
