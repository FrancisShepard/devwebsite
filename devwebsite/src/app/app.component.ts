import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'devwebsite';
  isLogged = false;

  constructor() { }

  ngOnInit() {
  }

  swapStatus() {
    if (this.isLogged === false) {
      this.isLogged = true;
    } else {
      this.isLogged = false;
    }
    console.log(this.isLogged);
  }
}
