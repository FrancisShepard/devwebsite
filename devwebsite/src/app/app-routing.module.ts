import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { ProfilesComponent } from './components/profiles/profiles.component';
import { ProfileComponent } from './components/profile/profile.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CreateProfileComponent } from './components/create-profile/create-profile.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'components/register', component: RegisterComponent},
  {path: 'components/login', component: LoginComponent},
  {path: 'components/profiles', component: ProfilesComponent},
  {path: 'components/profile', component: ProfileComponent},
  {path: 'components/dashboard', component: DashboardComponent},
  {path: 'components/create-profile', component: CreateProfileComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
